@extends('layout/main')

@section('container')
<div class="piagam">
          <div class="row text-center mb-3">
            <div class="col fst-italic" data-aos="fade-down" data-aos-delay="100">
              <h3>My Achievement</h3>
            </div>
          </div>
          <div class="row ms-2" style="gap: 70px; align-item:center; justify-content:center;">
            <!-- max 12 space sehingga menggunakan 4 = 12 : 4 sehingga menampung 3 baris colom -->
            <!-- 1 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="300">
                <div class="card-body">
                  <p class="card-text text-center fw-bold">{{ $text1 }}</p>
                </div>
                <img src="img/dddd.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
            <!-- 2 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="400">
                <div class="card-body" >
                  <p class="card-text text-center fw-bold">{{ $text2 }}</p>
                </div>
                <img src="img/p2.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
            <!-- 3 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="500">
                <div class="card-body">
                  <p class="card-text text-center fw-bold">{{ $text3 }}</p>
                </div>
                <img src="img/p3.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
            <!-- 4 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="600">
                <div class="card-body">
                  <p class="card-text text-center fw-bold">{{ $text4 }}</p>
                </div>
                <img src="img/p4.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
            <!-- 5 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="700">
                <div class="card-body">
                  <p class="card-text text-center fw-bold">{{ $text5 }}</p>
                </div>
                <img src="img/p5.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
            <!-- 6 -->
            <div class="col-md-3 mb-3" style="box-shadow: 0 4px 8px 0 rgb(23, 20, 236), 0 6px 20px 0 rgb(23, 20, 236);">
              <div class="card" data-aos="zoom-in" data-aos-delay="800">
                <div class="card-body">
                  <p class="card-text text-center fw-bold">{{ $text6 }}</p>
                </div>
                <img src="img/p6.png" class="card-img-top" alt="hobi1" />
                <div class="card-body">
                  <a style="text-decoration:none" href="" class="card-text text-center fw-bold">{{ $text7 }}</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
    </div>
@endsection