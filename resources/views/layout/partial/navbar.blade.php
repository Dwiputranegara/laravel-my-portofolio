    <header id="header">
        <div class="d-flex flex-colum">
            <img src="img/ffg.png" alt="" width="80" height="80">
            <p class="fs-3 fw-bold fst-italic text-light" style="margin-top: 20px ">DPN<span style="color: aqua">profile</span></p>
        </div>
        <div class="menu fw-bold fst-italic fixed-top" >
            <ul class="navbar-nav">
                <li class="nav-item pb-3">
                    <a class= "nav-link {{ ($title === "Home") ? 'active' : '' }}" style="color: white;
                    text-shadow: 2px 2px 4px rgb(7, 26, 192), 0 0 25px blue, 0 0 5px darkblue;" href="/"><i class="bi bi-house-door me-2 "></i>Home</a>
                </li>
                <li class="nav-item pb-3 text-light">
                    <a class="nav-link {{ ($title === "About") ? 'active' : '' }}" style="color: white;
                    text-shadow: 2px 2px 4px rgb(7, 26, 192), 0 0 25px blue, 0 0 5px darkblue;" href="/about"><i class="bi bi-person me-2 "></i>About</a>
                </li>
                <li class="nav-item pb-3">
                    <a class="nav-link {{ ($title === "Prestasi") ? 'active' : '' }}" style="color: white;
                    text-shadow: 2px 2px 4px rgb(7, 26, 192), 0 0 25px blue, 0 0 5px darkblue;" href="/prestasi"><i class="bi bi-trophy me-2"></i>Achievement</a>
                </li>
                <li class="nav-item pb-3">
                    <a class="nav-link {{ ($title === "Contact") ? 'active' : '' }}" style="color: white;
                    text-shadow: 2px 2px 4px rgb(7, 26, 192), 0 0 25px blue, 0 0 5px rgb(8, 8, 136);" href="/kontak"><i class="bi bi-envelope-open me-2 "></i>Contact</a>
                </li>
            </ul>
        </div>
        <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>
        <div class="nengae">
            <ul class="copyright text-light fw-light nav-link">
                <p class="text-light fs-6">&copy; <i>2021 DPN</i><span style="color: aqua"><i>profile</i></span></p>
                <p class="huss text-light  fs-6 mt-n2">Created With <i class="bi bi-heart-fill text-danger"></i> By
                    <a class: href="https://www.instagram.com/_d.p.n/"><i><b>Dwi Putra Negara</b></i></a></p>
            </ul>
        </div>
    </header> 