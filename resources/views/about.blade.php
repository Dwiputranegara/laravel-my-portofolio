@extends('layout/main')

@section('container')
    <div class="slide">
        <div class="slide1"></div>
        <div class="slide2"></div>
        <div class="slide3"></div>
    </div>
    <div class="me fw-bold fst-italic text-light" data-aos="fade-left" data-aos-delay="50">
        <h1>{{ $Tname }}</h1>
        <div class="isi fw-bold fst-italic text-light" data-aos="fade-left" data-aos-delay="100">
            <p>{{ $Abtme }}</p>
        </div>
    </div>
    <div class="bahasa" data-aos="fade-right" data-aos-delay="450">
        <h1>Leanguage</h1>
    </div>
    <div class="list" data-aos="fade-right" data-aos-delay="500">
        <dl class="list-grup py-3">
            <dt class="list-group-inline pb-3 "><i class="bi bi-book-half px-3 text-warning"></i>English</dt>
            <dt class="list-group-inline pb-3 "> <i class="bi bi-book-half px-3 text-success"></i>Indonesia</dt>
            <dt class="list-group-inline pb-3 "> <i class="bi bi-book-half px-3 text-success"></i>Bali</dt>
        </dl>
    </div>
    <div class="work" data-aos="fade-left" data-aos-delay="450">
        <h1>Skills</h1>
    </div>
    <div class="skil" data-aos="fade-left" data-aos-delay="500">
        <dl class="list-grup py-3">
            <dt class="list-group-inline pb-3 me-1">Bootstrap <i class="bi bi-bootstrap"></i></dt>
            <dt class="list-group-inline pb-3">HTML<i class="fab fa-html5 ms-3 me-2"></i></dt>
            <dt class="list-group-inline pb-3">CSS<i class="fab fa-css3-alt ms-3 me-2"></i></dt>
            <dt class="list-group-inline pb-3">PHP<i class="fab fa-php ms-3 me-2"></i></dt>
        </dl>
    </div>
    <div class="leser">
        
    </div>
@endsection