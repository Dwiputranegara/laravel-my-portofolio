@extends('layout/main')

@section('container')
<div class="kontk text-light">
    <!-- kontak -->
    <div class="row text-center" data-aos="zoom-in-up" data-aos-delay="150">
        <div class="col">
            <h3 class="fst-italic" style="font-size: 50px;
            color: white;
            text-shadow: 3px 3px 4px black, 0 0 35px blue, 0 0 10px darkblue;">Contact Me</h3>
                <p class="asui"><i class="bi bi-suit-heart-fill text-danger me-2"></i>Send Your Response About My Website<i class="bi bi-suit-heart-fill text-danger ms-2"></i></p>
        </div>
    </div>
    <div class="row justify-content-center"  data-aos="flip-left" data-aos-delay="550">
        <div class="col-md-6">
            <form>
                <div class="mb-3">
                    <label for="name" class="form-label">Full Name</label>
                      <input type="text" class="form-control" placeholder="Input Your Full Name!" id="name" aria-describedby="name" />
                    </div>
                    <div class="mb-3">
                      <label for="email" class="form-label">E-mail</label>
                      <input type="email" class="form-control" placeholder="Input Your Email!" id="email" aria-describedby="email" />
                    </div>
                    <div class="mb-3">
                      <label for="pesan" class="form-label">Message</label>
                      <textarea class="form-control" id="pesan" placeholder="Send Your Message!" rows="4"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success px-4"><b>Send <i class="bi bi-check2 ms-2"></i></b></button>

                    <!-- button sosmed -->
                    <div class="pt-4 fs-5 rounded-circle text-center">
                      <button class="btn btn-primary px-3 mx-3">
                        <a href="https://web.facebook.com/dwiputra.negara.35" class="text-white fw-bold text-primary">Facebook <i class="bi bi-facebook"></i></a>
                      </button>
                      <button class="btn btn-warning px-3 mx-3 mt-2">
                        <a href="https://www.instagram.com/_d.p.n/" class="text-white fw-bold">Instagram <i class="bi bi-instagram"></i></a>
                      </button>
                      <button class="btn btn-danger px-3 mx-3 mt-2">
                        <a href="https://www.youtube.com/channel/UCd7DcuEzHORFjMqSqCoheLA" class="text-white fw-bold">Youtube <i class="bi bi-youtube"></i></a>
                      </button>
                      <!-- button sosmed -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection