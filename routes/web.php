<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AbooutController;
use App\Http\Controllers\PrestasiControler;
use App\Http\Controllers\KontakController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'homee']);

Route::get('/about',[AbooutController::class, 'aboutt']);

Route::get('/prestasi',[PrestasiControler::class, 'prestasii']);

Route::get('/kontak',[KontakController::class, 'kontakk']);

